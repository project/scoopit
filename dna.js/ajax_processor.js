﻿/*
 *
 This a function that is used to retrieve all local content type using the field selected
 *
 */
function getLocalContentTypeFields(obj) {
    try {
        var form = null;
        var tempFeed = document.getElementById("local_field");
        var installationRoot = document.getElementById("dna_server_root").value;
        var feedbackdiv = tempFeed.id;
        var param = 'content-type=' + obj.value;
        var url_arg = installationRoot + 'scoopit-api/get-local-content-type';
        var code = 150;
        var reqType = null;
        var func = 'getRemoteContentTypeFields("remote_type")';
        var data = null;
        new function () {
            callprocess(code, form, feedbackdiv, param, url_arg, data, reqType, func);
        }
    } catch (E) {
    }
}

/*
 *
 This a function that is used to retrieve all remote scoop it content type using the object id specified
 *
 */
function getRemoteContentTypeFields(objId) {
    try {
        //alert(objId);
        var installationRoot = document.getElementById("dna_server_root").value;
        var obj = document.getElementById(objId);
        var form = null;
        var tempFeed = document.getElementById("remote_field");
        var localType = document.getElementById("local_type");
        if (obj.value == null || localType.value == null || obj.value == '' || localType.value == '') {
            return;
        }
        var feedbackdiv = tempFeed.id;
        var param = 'content-type=' + obj.value + '&local-type=' + localType.value;
        var url_arg = installationRoot + 'scoopit-api/get-remote-content-type';
        var code = 150;
        var reqType = null;
        var func = null;
        var data = null;
        new function () {
            callprocess(code, form, feedbackdiv, param, url_arg, data, reqType, func);
        }
    } catch (E) {
    }
}

/*
 *
 This a function that is used to save the Role of Author in Drupal install.
 *
 */
function saveScoopitAuthor(obj) {
    try {
        var installationRoot = document.getElementById("dna_server_root").value;
        var form = null;
        if (obj.value == null) {
            return;
        }
        var feedbackdiv = 'authorFeedBack';
        var param = 'scoopItUserSel=' + obj.value;
        var url_arg = installationRoot + 'scoopit-api/save-author-user';
        var code = 150;
        var reqType = true;
        var func = null;
        var data = null;
        new function () {
            callprocess(code, form, feedbackdiv, param, url_arg, data, reqType, func);
        }
    } catch (E) {
    }
}

/*
 *
 Remove the GIF proccessing image
 *
 */

function removeProcessingImage() {
    removeElement('proceImage');
}

/*
 *
 This Function to reload the page when its needed.
 *
 */

function refreshPage() {

    window.location.reload(true);
}
