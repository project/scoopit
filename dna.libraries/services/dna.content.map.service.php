<?php

/*
 *
 * The Map class for Mapping services between scoopit platform and Drupal entities
 * */

class DnaContentMapService extends DnaMainService
{
    /*
     * class variable(s)
     */
    private static $instance = NULL;

    //this is the singleton function
    public static function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //constructor
    public function __construct()
    {
        parent::__construct();
    }

    //function to get the maps stored based on the local and remote
    public function getMaps($localContentType, $remoteContentType)
    {

        $stmt = db_query("SELECT * FROM {scoopit_api_content_map} WHERE local_type = :local_type and remote_type = :remote_type",
            array(':local_type' => $localContentType,
                ':remote_type' => $remoteContentType,
            )
        );
        $data = $stmt->fetchAll();

        return $data;
    }

    //get the maps by local type
    public function getMapsByLocalContentType($localContentType)
    {

        $stmt = db_query("SELECT * FROM {scoopit_api_content_map} WHERE local_type = :local_type ;",
            array(
                ':local_type' => $localContentType,
            )
        );
        $data = $stmt->fetchAll();


        return $data;
    }

    //get the maps by remote content type
    public function getMapsByRemoteContentType($remoteContentType)
    {

        $stmt = db_query("SELECT * FROM {scoopit_api_content_map} WHERE remote_type = :remote_type ;",
            array(
                ':remote_type' => $remoteContentType,
            )
        );
        $data = $stmt->fetchAll();

        return $data;
    }

    //get the already mapped local type using the remote object type
    public function getLocalTypeFromRemoteObject($dataObject)
    {
        $remoteContentType = $dataObject->scoopit_type;
        $localObjectType = NULL;
        //get the first object local type you find bound to this type of remote object
        $tempMaps = $this->getMapsByRemoteContentType($remoteContentType);
        if (is_array($tempMaps) && !empty($tempMaps)) {
            foreach ($tempMaps as $tempMap) {
                $localObjectType = $tempMap->local_type;
                break;
            }

        }

        return $localObjectType;
    }

    //get the already mapped remote type using the local type
    public function getRemoteTypeFromLocalObject($dataNode)
    {
        $localObjectType = $dataNode->type;
        $remoteContentType = NULL;
        //get the first object remote type you find bound to this type of local object
        $tempMaps = $this->getMapsByLocalContentType($localObjectType);
        if (is_array($tempMaps) && !empty($tempMaps)) {
            foreach ($tempMaps as $tempMap) {
                $remoteContentType = $tempMap->remote_type;
                break;
            }

        }

        return $remoteContentType;
    }

    //Map a local object to remote object
    public function mapLocalObjectToRemoteObject($dataNode, $remoteObjectClass)
    {
        $localContentType = $dataNode->type;

        $fieldInfos = field_info_instances("node", $localContentType);

        $mappingObjects = $this->getMaps($localContentType, $remoteObjectClass);
        $retObj = NULL;

        if (is_array($mappingObjects) && !empty($mappingObjects)) {
            $remoteObjectClassName = "Scoopit\\Entities\\" . $remoteObjectClass;
            $remoteObject = new $remoteObjectClassName();

            foreach ($mappingObjects as $mappingObject) {
                $remote_field_name = $mappingObject->remote_field_name;
                $local_field_name = $mappingObject->local_field_name;

                if ($local_field_name == 'drupal_content_type_mapper') {
                    continue;
                }

                $fieldInfo = NULL;

                foreach ($fieldInfos as $fieldInfo) {
                    //...
                    if ($fieldInfo['field_name'] == $local_field_name)
                        break;
                }

                $nodeField = $dataNode->$local_field_name;

                //if($local_field_name=='field_image')
                if (trim($fieldInfo['widget']['type']) == 'image_image' || trim($fieldInfo['widget']['module']) == 'image') {
                    $remoteObjectImageClassName = "Scoopit\\Entities\\Sub\\Image";
                    $remoteImageObject = new $remoteObjectImageClassName();
                    $tmpImages = $nodeField[$this->pick_field_language($dataNode->language, 'image')];
                    foreach ($tmpImages as $tempImage) {
                        $img_url = $tempImage['uri'];
                        break;
                    }
                    $wrapper = file_stream_wrapper_get_instance_by_uri('public://');
                    $doc_root = $_SERVER["DOCUMENT_ROOT"];
                    $realpath = str_replace($doc_root, '', $wrapper->realpath());
                    $remoteImageObject->url = (SCOOPIT_API_LOCAL_SERVER) . str_replace('public:/', $realpath, $img_url);//site url to the image

                    $remoteImageObject->url = SCOOPIT_API_SERVER_SCHEME . str_replace(array('///', '//'), '/', $remoteImageObject->url);
                    $tmpImages = $nodeField[/*$node->language*/
                    $this->pick_field_language($dataNode->language, 'image')];
                    foreach ($tmpImages as $tempFid) {
                        $fid = $tempFid['fid'];
                        $remoteImageObject->id = $fid;
                        break;
                    }

                    unset($remoteImageObject->scoopit_type);
                    if ($remoteImageObject->id != NULL) {
                        $remoteObject->$remote_field_name = $remoteImageObject;
                    } else {
                        $remoteObject->$remote_field_name = NULL;
                    }
                    continue;
                } else if ($this->isTaxonomyField($fieldInfo)) {
                    $t = $dataNode->$local_field_name;
                    $tags = $t[$this->pick_field_language($dataNode->language, 'taxonomy')];
                    $tagsRet = array();
                    foreach ($tags as $tag) {
                        $taxonomy = taxonomy_term_load($tag['tid']);
                        $tagsRet[] = $taxonomy->name;
                    }

                    $remoteObject->$remote_field_name = $tagsRet;
                    continue;
                }

                $contentField = $dataNode->$local_field_name;

                if (isset($contentField[$this->pick_field_language($dataNode->language, 'body')][0]['value'])) {

                    $tmpContent = $contentField[$this->pick_field_language($dataNode->language, 'body')];
                    foreach ($tmpContent as $tempBody) {
                        $body = $tempBody['value'];
                        $remoteObject->$remote_field_name = $body;
                        $bodySummary = $tempBody['summary'];
                        $remoteObject->summary = $bodySummary;

                        break;
                    }

                } else {
                    $remoteObject->$remote_field_name = $dataNode->$local_field_name;
                }

            }

            $remoteObject->local_object_id = $dataNode->nid;


            if (isset($dataNode->title))
                $remoteObject->title = $dataNode->title;
            $remoteObject->local_type = $localContentType;
            $remoteObject->state = ($dataNode->status == 1) ? 'published' : 'scheduled';
            $remoteObject->scoopit_language = $dataNode->language;
            $remoteObject->id = $dataNode->field_scoopit_id[$dataNode->language][0]['value'];//force users to create this field
            $remoteObject->publicationDate = date('Y-m-d H:i:s O', $dataNode->created);
            $remoteObject->promoteState = $dataNode->promote == 1 ? 'promoted' : 'notPromoted';


            $remoteObject->url = SCOOPIT_API_LOCAL_SERVER . '/node/' . $dataNode->nid;
            $remoteObject->url = SCOOPIT_API_SERVER_SCHEME . str_replace(array('///', '//'), '/', $remoteObject->url);


            //removing unwanted fields from the scoopit
            if (isset($remoteObject->scoopit_type))
                unset($remoteObject->scoopit_type);

            if (isset($remoteObject->local_type))
                unset($remoteObject->local_type);

            if (isset($remoteObject->local_object_id))
                unset($remoteObject->local_object_id);


            $remoteObject->id = $dataNode->nid;//force node to default to local field id

            $user = user_load($dataNode->uid);
            $username = $user->name;

            $remoteObject->author = $username;//force node to pick username for author

            $retObj = $remoteObject;
        } else {
            $retObj = NULL;
        }

        return $retObj;
    }

    //Map a remote object to local object for action
    public function mapRemoteObjectToLocalArgumentForAction($dataObject)
    {
        $dataObject->scoopit_type = "Post";
        $remoteContentType = $dataObject->scoopit_type;
        $localObjectType = ($dataObject->local_type && trim($dataObject->local_type) != '') ? $dataObject->local_type : $this->getLocalTypeFromRemoteObject($dataObject);
        $localObjectId = (int)"0" . $dataObject->id;

        if ($localObjectType == NULL) {
            return NULL;
        }

        $mappingObjects = $this->getMaps($localObjectType, $remoteContentType);
        $retObj = NULL;

        if (is_array($mappingObjects) && !empty($mappingObjects)) {
            $nodeData = new \stdClass();
            $nodeFieldValues = array();
            foreach ($mappingObjects as $mappingObject) {
                $remote_field_name = $mappingObject->remote_field_name;
                $local_field_name = $mappingObject->local_field_name;

                $nodeFieldValues[$local_field_name] = $dataObject->$remote_field_name;
            }

            $nodeFieldValues['field_scoopit_id'] = $dataObject->id;

            if (isset($dataObject->publicationDate))
                $nodeFieldValues['publicationDate'] = $dataObject->publicationDate;

            if (isset($dataObject->summary))
                $nodeFieldValues['scoopit_summary'] = $dataObject->summary;


            if (isset($dataObject->title))
                $nodeData->node_title = $dataObject->title;

            $nodeData->node_content_type = $localObjectType;
            $nodeData->node_field_values = $nodeFieldValues;
            if (isset($dataObject->promoteState)) {
                $nodeData->promoteState = $dataObject->promoteState;
            } else {
                $nodeData->promoteState = "promoted";
            }
            $nodeData->comment_disabled = "0";//to be determined later
            $nodeData->local_object_id = $localObjectId;//to be determined later
            $nodeData->state = $dataObject->state;//(==1)?'published':'scheduled';
            $nodeData->scoopit_language = $dataObject->scoopit_language;//language;

            $retObj = $nodeData;
        } else {
            $retObj = NULL;
        }

        return $retObj;
    }

}
