<?php

/**
 * An image Scoopit Class (Sub entity to the post class)
 */
namespace Scoopit\Entities\Sub;

class Image
{
    public $id;
    // long - the id of the post (Integer & Unique / mandatory) Unique way to identified an image
    public $url;
    // string - original url of the post (String / mandatory) Image url on Drupal side
    public $scoopit_type = "Image";
}