<?php

/*
 * Post Entity Class for Scoop.It post object.
 *
 * */
namespace Scoopit\Entities;

class Post
{
    public $id;
    // long - the id of the post (Integer & Unique / mandatory)
    public $publicationDate;
    //timestamp - the publication date of the original article (Date / mandatory)
    public $url;
    // string - original url of the post (String / mandatory)
    public $title;
    // string - the post title
    public $summary;
    // string - post summary in plain text
    public $state;
    // Could be published or scheduled
    public $image;
    // The image of the Scoop.it Post - ideally uploaded on the Drupal blog when content is created on SCD side
    public $tags;
    // topic_tag[] - array of tags
    public $scoopit_type = "Post";
    // string - the scoop it type to identify the remote object
    public $local_type;
    // string - the local drupal type to identify the local object to target
    public $local_object_id = 0;
    //long - for identifying local object
    public $content;
    // string - post content in plain text
    public $author;
    // string - post content in plain text
}